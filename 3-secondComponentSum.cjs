let arraysLogin = require("./2-arrays-logins.cjs");

//     3. Find the sum of all the second components of the ip addresses.
function findSecondComponent(arraysLogin) {
    let sumOfAllSecondComponentIp = arraysLogin.reduce((ansVariable, givenObj) => {
        let secondComponentSum = 0;
        let dotCount = 0;
        for (let digit of givenObj.ip_address) {
            if (digit == ".") {
                dotCount++;
            }
            if (dotCount == 1 && digit != ".") {
                secondComponentSum += parseInt(digit);
            }
            if (dotCount > 1) {
                break;
            }
        };
        ansVariable += secondComponentSum;
        return ansVariable;
    }, 0);
    return sumOfAllSecondComponentIp;

}
console.log(findSecondComponent(arraysLogin))