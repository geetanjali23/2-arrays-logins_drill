// 7. Calculate how many .org, .au, .com emails are there
let arraysLogin = require("./2-arrays-logins.cjs");
function typesOfEmails(arraysLogin) {
    let domainEmailsList = {};
    arraysLogin.map((item) => {
        if (item.email.slice(-4) == ".org") {
            if (domainEmailsList[".org"]) {
                domainEmailsList[".org"]++;
            }
            else {
                domainEmailsList[".org"] = 1;
            }
        }
        else if (item.email.slice(-3) == ".au") {
            if (domainEmailsList[".au"]) {
                domainEmailsList[".au"]++;
            }
            else {
                domainEmailsList[".au"] = 1;
            }
        }
        if (item.email.slice(-4) == ".com") {
            if (domainEmailsList[".com"]) {
                domainEmailsList[".com"]++;
            }
            else {
                domainEmailsList[".com"] = 1;
            }
        }
    });
    return domainEmailsList;
}
console.log(typesOfEmails(arraysLogin));