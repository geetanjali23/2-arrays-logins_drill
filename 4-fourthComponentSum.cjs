
//     4. Find the sum of all the fourth components of the ip addresses.
let arraysLogin = require("./2-arrays-logins.cjs");

function findFourthComponent(arraysLogin) {
    let sumFourthComponent = arraysLogin.reduce((ansVariable, eachData) => {
        let dotCount = 0;
        let sumFourth = 0;
        for (let idxValue of eachData.ip_address) {
            if (idxValue == ".") {
                dotCount++;
            }
            if (dotCount === 3 && idxValue != ".") {
                sumFourth += parseInt(idxValue);
            }
            if (dotCount > 3) {
                break;
            }
        }
        ansVariable += sumFourth;
        return ansVariable;
    }, 0);
    return sumFourthComponent;
}
console.log(findFourthComponent(arraysLogin));