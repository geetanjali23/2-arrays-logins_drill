// 1. Find all people who are Agender
let arraysLogin = require("./2-arrays-logins.cjs");
function agender(arraysLogin) {

    // let allAgenderPeople = arraysLogin.filter((item) => {
    //     return (item.gender === "Agender");
    // });
    // return allAgenderPeople;

    // Solved Using reduce & Object.assign
    // let agenderList = arraysLogin.reduce((emptyArr, itemsArrObj) => {
    //     if (itemsArrObj.gender == "Agender") {
    //         emptyArr.push(Object.assign({},itemsArrObj));
    //     }
    //     return emptyArr;
    // }, [])
    // return agenderList;

    let agenderPeople = [];
    for(let i = 0; i < arraysLogin.length; i++) {
        let arrObj = arraysLogin[i];
        let gender = arrObj["gender"];
        if(gender == "Agender") {
            agenderPeople.push(arrObj);
        }
    }
    return agenderPeople;
}
console.log(agender(arraysLogin), "printing ans........");