// 7. Sort the data in descending order of first name
let arraysLogin = require("./2-arrays-logins.cjs");
function sortDescending(arraysLogin) {

    let sortedNameData = arraysLogin.sort((person_1, person_2) => {
        let obj_1 = person_1.first_name;
        let obj_2 = person_2.first_name;
        if (obj_1 > obj_2) {
            return -1;
        }
    });
    return sortedNameData;
}
console.log(sortDescending(arraysLogin), "printing result");