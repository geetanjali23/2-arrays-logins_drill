let arrayData = require("./2-arrays-logins.cjs");

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
function split_IP_Address(arrayData) {
    let ipComponentsList = arrayData.reduce((emptyArr, itemObj) => {
        let ipComponents = [];
        let ipPart = "";
        for (let digit in itemObj.ip_address) {
            if (itemObj.ip_address[digit] == ".") {
                ipComponents.push(parseInt(ipPart));
                ipPart = "";
            }
            else if (digit == itemObj.ip_address.length - 1) {
                ipPart += itemObj.ip_address[digit];
                ipComponents.push(parseInt(ipPart));
            }
            else {
                ipPart += itemObj.ip_address[digit];
            }
        };
        let newIpObject = Object.assign({}, itemObj);
        newIpObject.ip_address = ipComponents;
        emptyArr.push(newIpObject);
        return emptyArr;
    }, []);

    return ipComponentsList;

    // arrayData.forEach((element) => {
    //     element.ip_address = element["ip_address"].split(".").map((eachIndex) => Number(eachIndex));
    // });
    // return arrayData;
}
console.log(split_IP_Address(arrayData), "Printing ans!...");