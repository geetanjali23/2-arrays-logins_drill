// 6. Filter out all the .org emails
let arraysLogin = require("./2-arrays-logins.cjs");

function filterEmails(arraysLogin) {
    let orgEmails = arraysLogin.reduce((ansArr, givenArrObj) => {
        if (givenArrObj.email.slice(-4) == ".org") {
            ansArr.push({ email: givenArrObj.email })
        }
        return ansArr;
    }, []);

    return orgEmails;

    // let filteredObject = arraysLogin.filter((eachData) => {
    //     console.log(eachData.email.slice(-4), "Each%%%%%% DAta");
    //     return (eachData.email.slice(-4) === ".org");
    // });
    // return filteredObject;

}
console.log(filterEmails(arraysLogin));
